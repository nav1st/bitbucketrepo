package com.poc.regservice.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.poc.regservice.model.User;

@Repository
@Transactional(readOnly=true)
public class CustomRepoImpl implements CustomRepo{
	
	@PersistenceContext
    EntityManager entityManager;

	@Override
	public List<User> loggedinUser(String userName, String password) {
		
		User user;
		
		Query query = entityManager.createNativeQuery("SELECT user.* FROM user as user " +
                "WHERE user.username LIKE ? and user.username LIKE ?", User.class);
        query.setParameter(1, userName + "%");
        query.setParameter(2, password + "%");
        return query.getResultList();
	}

	@Override
	public Integer transferFund(int user1, int user2, int amount) {
		User user;
		Query query = entityManager.createNativeQuery("SELECT user.* FROM user as user " +
                "WHERE user.id=?", User.class);
        query.setParameter(1, user1 + "%");
        User from = (User) query.getSingleResult();
        
        query = entityManager.createNativeQuery("SELECT user.* FROM user as user " +
                "WHERE user.id=?", User.class);
        query.setParameter(1, user2 + "%");
        User to = (User) query.getSingleResult();
        
        if(from !=null && to !=null ) {
	        if(from.getBalance() > amount) {
	        	from.setBalance(from.getBalance() - amount);
	        	to.setBalance(to.getBalance() + amount);
	        }
			
			query = entityManager.createNativeQuery("update User  set  balance=" + from.getBalance() +
	                "WHERE user.id=?", User.class);
	        query.setParameter(1, user1);
	        int count1 = query.executeUpdate();
	        
	        int count2 = 0;
	        if(count1>0){
	        query = entityManager.createNativeQuery("update User  set  balance=" + to.getBalance() +
	                "WHERE user.id=?", User.class);
	        query.setParameter(1, user2);
	        count2 = query.executeUpdate();
	        }
	        if(count1>0 && count2>0) {
	        	return count1;
	        }
        }
        
        return 0;
        
	}
	
	

}
