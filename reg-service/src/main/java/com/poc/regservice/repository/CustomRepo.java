package com.poc.regservice.repository;

import java.util.List;

import com.poc.regservice.model.User;

public interface CustomRepo {

	List<User> loggedinUser(String userName, String password);

	Integer transferFund(int user1, int user2, int amount);

}
